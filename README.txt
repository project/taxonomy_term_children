
-- INSTALLATION --

Enable the module like any other Drupal Module /admin/build/modules


-- CONFIGURATION --

1.) Create a Vocabulary and add some terms
2.) Create the content and assign terms to the nodes

-- REQUIREMENTS --

* Taxonomy module enabled   

-- ADDITIONAL MODULES SUGESTION --

The following modules fit best to improve the workflow to add Images to the horizontal slider.

* taxonomy_menu

    
 -- THEMING --
 
The provided base css is sufficient to demonstrate the functionality (tested with Aquia Marina)
Take a look at the taxonomy_term_children.css for some css hints.


-- CONTACT --

Current maintainers:
* Marco Kleine-Albers (e-anima) -> http://drupal.org/user/255202


This project has been sponsored by:
* ARTWAVES
* http://www.artwaves.de for more information.
